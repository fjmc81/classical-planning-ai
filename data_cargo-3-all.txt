uninformed
__________________________________________
__________________________________________
(aind) bash-3.2$ python run_search.py -p 3 -s 1
Solving Air Cargo Problem 3 using breadth_first_search...

# Actions   Expansions   Goal Tests   New Nodes
    88        14663       18098       129625

Plan length: 12  Time elapsed in seconds: 9.93210371733995
__________________________________________
(aind) bash-3.2$ python run_search.py -p 3 -s 2

Solving Air Cargo Problem 3 using depth_first_graph_search...

# Actions   Expansions   Goal Tests   New Nodes
    88         408         409         3364

Plan length: 392  Time elapsed in seconds: 1.4568490419987938
__________________________________________


h_greedy
__________________________________________
__________________________________________

(aind) C:\__projects\udacity\classical-planning-ai>python run_search.py -p 3 -s 4

Solving Air Cargo Problem 3 using greedy_best_first_graph_search with h_unmet_goals...

# Actions   Expansions   Goal Tests   New Nodes
    88          25          27         230

Plan length: 15  Time elapsed in seconds: 0.030432215796703636
__________________________________________
(aind) bash-3.2$ python run_search.py -p 3 -s 5

Solving Air Cargo Problem 3 using greedy_best_first_graph_search with h_pg_levelsum...

# Actions   Expansions   Goal Tests   New Nodes
    88          14          16         126

Plan length: 14  Time elapsed in seconds: 30.648987207001483
__________________________________________
(aind) bash-3.2$ python run_search.py -p 3 -s 6

Solving Air Cargo Problem 3 using greedy_best_first_graph_search with h_pg_maxlevel...

# Actions   Expansions   Goal Tests   New Nodes
    88          21          23         195

Plan length: 13  Time elapsed in seconds: 36.790391979997366
__________________________________________
(aind) C:\__projects\udacity\classical-planning-ai>python run_search.py -p 3 -s 7

Solving Air Cargo Problem 3 using greedy_best_first_graph_search with h_pg_setlevel...

# Actions   Expansions   Goal Tests   New Nodes
    88          35          37         345

Plan length: 17  Time elapsed in seconds: 60.83645896540484


h_A*
__________________________________________
__________________________________________
(aind) bash-3.2$ python run_search.py -p 3 -s 8

Solving Air Cargo Problem 3 using astar_search with h_unmet_goals...

# Actions   Expansions   Goal Tests   New Nodes
    88         7388        7390       65711

Plan length: 12  Time elapsed in seconds: 11.40926194900021
__________________________________________
franks-Air:classical-planning-ai frank$ python run_search.py -p 3 -s 10

Solving Air Cargo Problem 3 using astar_search with h_pg_maxlevel...

# Actions   Expansions   Goal Tests   New Nodes
    88         9580        9582       86312

Plan length: 12  Time elapsed in seconds: 5800.034700579001
__________________________________________
(aind) bash-3.2$ python run_search.py -p 3 -s 11

Solving Air Cargo Problem 3 using astar_search with h_pg_setlevel...

# Actions   Expansions   Goal Tests   New Nodes
    88         3423        3425       31596

Plan length: 12  Time elapsed in seconds: 12372.269063723308
